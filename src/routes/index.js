import React from 'react'
import { Switch, Route } from 'react-router-dom'

import List from '../pages/List';
import Create from '../pages/Create'

const Routes = () => (
  <Switch>
    <Route path="/" exact component={List} />
    <Route path="/create" component={Create} />
    <Route path="/edit/:id" component={Create} />
  </Switch>
)

export default Routes
