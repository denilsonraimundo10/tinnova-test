import styled, { css } from 'styled-components'

export const InputText = styled.input`
  background-color: transparent;
  border-bottom: 1px solid;
  color: #333333;
  height: 50px;
  padding-left: 8px;
  padding-right: 8px;

  ${props => (props.error ? 'border-color: #eb4a46' : 'border-color: #3b3b3b')};

  ${props => props.filled &&
    css`
      ~ .label {
        font-size: 12px;
        top: -16px !important;
      }
    `}

  &:focus {

    ~ .label {
      font-size: 12px;
      top: -16px;
    }
  }
`;

