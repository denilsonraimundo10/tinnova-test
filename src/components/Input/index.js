import React, { useState, useRef } from 'react'

import { InputText } from './styles'

const Input = ({ value, onChange, type = 'text', error }) => {
  const ref = useRef(null)

  const [filled, setFilled] = useState(!!value)

  function inputBlur() {
    setFilled(!!ref.current?.value)
  }

  return (
    <InputText
      type={type}
      placeholder=""
      value={value}
      filled={filled}
      ref={ref}
      onChange={event => onChange(event.target.value)}
      error={error}
      onBlur={inputBlur}
    />
  )
}

export default Input
