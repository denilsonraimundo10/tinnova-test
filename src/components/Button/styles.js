import styled, { css, keyframes } from 'styled-components'

const rotation = keyframes`
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
`

export const Button = styled.button`
  align-items: center;
  background: #00c8b3;
  border-radius: 25px;
  color: #fff;
  cursor: pointer;
  display: flex;
  font-weight: bold;
  height: 50px;
  justify-content: center;
  padding-left: 16px;
  padding-right: 16px;
  transition: background-color 0.5s;

  &:hover {
    opacity: .7;
  }

  ${props => props.isLoading &&
    css`

      img {
        display: block !important;
      }

      span {
        display: none !important;
      }
    `}

  img {
    animation: ${rotation} 2s infinite linear;
    display: none;
  }
`;
