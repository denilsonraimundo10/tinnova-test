import React from 'react'

import { Button } from './styles'

const Btn = ({ children, type="submit", loading }) => {

  return (
    <Button isLoading={loading} type={type}>{children}</Button>
  )
}

export default Btn
