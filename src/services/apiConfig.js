import axios from 'axios'

const apiConfig = axios.create({
  baseURL: 'https://private-9d65b3-tinnova.apiary-mock.com/'
})

export default apiConfig
