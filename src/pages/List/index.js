import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { FiTrash } from 'react-icons/fi'

import { Title, UsersList } from './styles'

import apiConfig from './../../services/apiConfig'

const List = () => {
  const [users, setUsers] = useState(() => {
    const storagedUsers = localStorage.getItem('@TinnovaTest:users')

    if (storagedUsers) return JSON.parse(storagedUsers)

    return []
  });

  useEffect(() => {
    const storagedUsers = localStorage.getItem('@TinnovaTest:users')

    if (!storagedUsers) {
      apiConfig.get('/users').then(response => {
        setUsers(response.data)

        localStorage.setItem(
          '@TinnovaTest:users',
          JSON.stringify(response.data),
        )
      }).catch(err => {
        console.log(err)
      })
    }
  }, [])

  function deleteUser(index) {
    const storagedUsers = JSON.parse(localStorage.getItem('@TinnovaTest:users'))

    storagedUsers.splice(index, 1)

    localStorage.setItem(
      '@TinnovaTest:users',
      JSON.stringify(storagedUsers),
    )

    setUsers(storagedUsers)
  }

  return (
    <>
      <Title>Listagem de usuários</Title>

      {
        users.length
        ? (
          <UsersList>
            {users.map((user, index) => (
              <li key={user.cpf} className="list-item">
                <Link className="link" to={`/edit/${index}`} title="Editar">{user.name} <br /> {user.email}</Link>

                <FiTrash title="Deletar" color="#eb4a46" size={24} onClick={() => deleteUser(index)} />
              </li>
              ))
            }
          </UsersList>
        )
        : <p style={{ textAlign: 'center', marginBottom: '32px' }}>Nenhum usuário foi encontrado</p>
      }

      <Link to="/create" style={{ display: 'block', textAlign: 'center', color: '#3154a4', fontWeight: 'bold'  }}>Cadastrar usuário</Link>
    </>
  )
}

export default List
