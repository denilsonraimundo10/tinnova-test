import styled from 'styled-components'

export const Title = styled.h1`
  margin: 32px auto;
  text-align: center;
`

export const UsersList = styled.ul`
  margin: 0 auto;
  max-width: 760px;

  .list-item {
    background: #fff;
    border-radius: 4px;
    display: flex;
    flex-direction: column;
    margin-bottom: 32px;
    padding: 24px;
    width: 100%;

    + .list-item {
      margin-top: 12px;
    }

    .link {
      color: #3154a4;
      font-weight: bold;
    }

    svg {
      cursor: pointer;
      margin-top: 12px;
    }
  }


  @media(min-width: 768px) {
    .list-item {
      align-items: center;
      justify-content: space-between;
      flex-direction: row;

      svg {
        cursor: pointer;
        margin-top: 0;
      }
    }
  }
`
