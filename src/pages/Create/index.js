import React, { useState } from 'react'
import * as Yup from 'yup'
import { useParams, Link } from "react-router-dom"

import imgLoading from './../../assets/loading.png'

import { Form, Title } from './styles'

import Input from './../../components/Input'
import Button from './../../components/Button'

const Create = () => {
  const { id } = useParams()

  const user = JSON.parse(localStorage.getItem('@TinnovaTest:users'))[id]

  const [name, setName] = useState(user?.name || '')
  const [email, setEmail] = useState(user?.email || '')
  const [cpf, setCpf] = useState(user?.cpf || '')
  const [phone, setPhone] = useState(user?.phone || '')
  const [errors, setErrors] = useState({})
  const [loading, setLoading] = useState(false)

  async function submitForm(event) {
    event.preventDefault()

    setLoading(true)
    setErrors({})

    const data = {
      name,
      email,
      cpf,
      phone
    }

    try {
      const schema = Yup.object().shape({
        name: Yup.string().min(3, 'Nome deve conter 3 caracteres ou mais'),
        email: Yup.string()
          .required('E-mail obrigatório')
          .email('Digite um e-mail válido'),
        cpf: Yup.string().length(11, 'CPF deve conter 11 caracteres'),
        phone: Yup.string().length(11, 'Telefone deve conter 11 caracteres (com DDD)'),
      });

      await schema.validate(data, {
        abortEarly: false,
      })

      const storagedUsers = JSON.parse(localStorage.getItem('@TinnovaTest:users'))

      setTimeout(() => {
        if (user) {
          storagedUsers[id] = data

          localStorage.removeItem('@TinnovaTest:users')
        } else {
          storagedUsers.push(data)

          setName('')
          setEmail('')
          setCpf('')
          setPhone('')
        }

        localStorage.setItem(
          '@TinnovaTest:users',
          JSON.stringify(storagedUsers),
        )

        setErrors({})

        setLoading(false)

        alert('Usuário salvo com sucesso!')
      }, 1000)

    } catch (err) {
      const objErrors = {}

      err.inner.forEach(error => {
        objErrors[error.path] = error.message;
      });

      setErrors(objErrors)
      setLoading(false)
    }
  }

  return (
    <>
      <Title>{user ? 'Editar' : 'Adicionar'} usuário</Title>

      <Form onSubmit={submitForm}>
        <div className="wrapper-input">
          <Input value={name} onChange={setName} error={ !!errors['name'] } />

          <label className="label">Nome completo (sem abreviações)</label>
          { errors['name'] && <p className="error-msg">{errors['name']}</p> }
        </div>

        <div className="wrapper-input">
          <Input value={email} onChange={setEmail} type="email" error={ !!errors['email'] } />

          <label className="label">E-mail</label>
          { errors['email'] && <p className="error-msg">{errors['email']}</p> }
        </div>

        <div className="wrapper-input">
          <Input value={cpf} onChange={setCpf} type="number" error={ !!errors['cpf'] } />

          <label className="label">CPF</label>
          { errors['cpf'] && <p className="error-msg">{errors['cpf']}</p> }
        </div>

        <div className="wrapper-input">
          <Input value={phone} onChange={setPhone} type="tel" error={ !!errors['phone'] } />

          <label className="label">Telefone</label>
          { errors['phone'] && <p className="error-msg">{errors['phone']}</p> }
        </div>

        <Button loading={loading}><span>{user ? 'Editar' : 'Cadastrar'}</span> <img src={imgLoading} alt="Loading" width="50" /></Button>
      </Form>

      <Link to="/" style={{ display: 'block', textAlign: 'center', color: '#3154a4', fontWeight: 'bold'  }}>Voltar</Link>
    </>
  )
}

export default Create
