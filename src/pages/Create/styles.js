import styled from 'styled-components'

export const Title = styled.h1`
  margin: 32px auto;
  text-align: center;
`
export const Form = styled.form`
  display: flex;
  flex-direction: column;
  margin: 32px auto;
  max-width: 420px;

  .wrapper-input {
    display: flex;
    flex-direction: column;
    margin-bottom: 32px;
    position: relative;

    .label {
      color: #9f9f9f;
      left: 8px;
      pointer-events: none;
      position: absolute;
      top: 16px;
      transition: all .25s;
    }

    .error-msg {
      color: #eb4a46;
      font-size: 12px;
      margin-top: 8px;
    }
  }
`;

