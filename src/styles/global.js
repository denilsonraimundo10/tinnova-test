import { createGlobalStyle } from 'styled-components'

const GlobalStyle = createGlobalStyle`
  * {
    box-sizing: border-box;
    margin: 0;
    outline: 0;
    padding: 0;
  }

  html, body {
    height: 100%;
  }

  body {
    background: #f0f0f5;
    color: #383838;

    -webkit-font-smoothing: antialiased;
  }

  body, input, button {
    font-family: Roboto, sans-serif;
    font-size: 16px;
  }

  input, button {
    border: none;
  }

  a {
    text-decoration: none;

    &:hover {
      opacity: .7;
    }
  }

  ul {
    list-style: none;
  }

  #root {
    height: 100%;
    padding: 0 10px;
    position: relative;
  }
`

export default GlobalStyle
