
## Tinnova Test

Este projeto foi criado com [Create React App](https://github.com/facebook/create-react-app).

### Configuração do projeto

- Para instalação dos pacotes, execute ``` yarn install ```.

### Executando o projeto

- Para rodar o projeto, execute ``` yarn start ```.  Por padrão, o projeto será executado na porta [:3000](http://localhost:3000)
